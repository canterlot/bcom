#include <stdint.h>
#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 


int main()
{

    char data[1025];
char data2[100];
    struct sockaddr_in server_ip;
	server_ip.sin_family = AF_INET;
	server_ip.sin_addr.s_addr = htonl(INADDR_ANY);
	server_ip.sin_port = htons(1489); 		/* this is the port number of running server */
    int32_t sockfd = socket(AF_INET, SOCK_STREAM, 0);
    int32_t client_connect = -1;

    int32_t res = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &(int32_t){1}, sizeof(int32_t));
    if (res == -1)
    {
        printf("Failed to set options\n");
        return -1;
    }
    bind(sockfd, (struct sockaddr*)&server_ip, sizeof(server_ip));
    listen(sockfd, 2);
    printf("Server running. Any clients?\n");
    int n;
    uint32_t counter = 0;


    while (1)
    {
        while (1)
        {
            client_connect = accept(sockfd, (struct sockaddr*)NULL, NULL);
            if (client_connect > 0)
            {
                break;
            }
            else
            {
               printf("Failed to connect.\n");
                sleep(1);
            }
        }
        snprintf(data, sizeof(data), "hej gustav? this is message %d", counter);
        send(client_connect, data, strlen(data), MSG_NOSIGNAL);
        /*n = recv(sockfd, data2, sizeof(data2), 0);
        if (n > 0)
        {
            printf("%s\n", data2);
        }*/
        counter++;
        close(client_connect);
        sleep(1);
    }
    return 0;
}
    
