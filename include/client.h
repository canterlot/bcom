#ifndef _BCOM_CLIENT_H_
#define _BCOM_CLIENT_H_

#include <stdint.h>
#include "bcom_types.h"


#define BCOM_LOCAL_IP "127.0.0.1"

void* bcom_client(struct msg_pipe_t *pipe);

//uint8_t send_all(int socket, void *buffer, size_t length, int flags);
//uint8_t recv_all(int socket, void *buffer, size_t length, int flags);


//void put_hex(uint8_t *data, unsigned int len, struct user_t user);

#endif
