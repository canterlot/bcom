#ifndef _BCOM_TERM_H_
#define _BCOM_TERM_H_

#include "bcom_types.h"

/* term.c prototypes */
void free_all(void);
void handler(int32_t sig);
int32_t tty_set_raw(void);
void tty_reset(void);


#endif
