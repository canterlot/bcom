#ifndef _BCOM_AES_H_
#define _BCOM_AES_H_

#include "bcom_types.h"

int8_t serialize(struct message_t *message, uint8_t *data);
int8_t deserialize(struct message_t *message, uint8_t *data);

#endif
