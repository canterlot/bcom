/*
 * TITLE: UI
 * DESC: everything to do with printing user_t interface to terminal
 * WHO TO BLAME: Vitalijs K,
 *
 * 
 */
#ifndef _BCOM_UI_H_
#define _BCOM_UI_H_

#if 0
#define NDEBUG
#endif

#include "bcom_types.h"
#include "string.h"

/* USER INPUT MACROS BEGIN */
#define COMMAND_CHAR    '!' /* character which decides if user is using commands */
/* USER INPUT MACROS END */

/* DISPLAY MACROS BEGIN */
#define FOOTER_ROWS 7U
#define HEADER_ROWS 1U
#define FILLER_ROWS (FOOTER_ROWS + HEADER_ROWS)  /* amount of rows which are not taken up by message space */
#define FILLER_COLS     11U /* extra columns excluding username on message line */
#define FILLER_COLS_IN  4U /* extra columns excluding username on input line */
/* DISPLAY MACROS END */

/* ANSI ESCAPE CODE MACROS BEGIN */
#define ANSI_UP(x)      printf("\033[%dA", x)   /* move cursor up x times*/
#define ANSI_DOWN(x)    printf("\033[%dB", x)   /* move cursor down x times */
#define ANSI_RIGHT(x)   printf("\033[%dC", x)   /* move cursor right x times */
#define ANSI_LEFT(x)    printf("\033[%dD", x)   /* move cursor left x times */

#define ANSI_ROW(x)     printf("\033[%dG", x)   /* move cursor to row x */
#define ANSI_XY(x, y)   printf("\033[%d;%dH", x, y) /* mover cursor to x, y */
#define ANSI_RESET()    printf("\033[H")        /* move cursor to 0,0 coord*/
#define ANSI_CLEARLN()  printf("\033[2K")       /* clear current line on which cursor resides */
#define ANSI_CLEAR()    printf("\033[J")        /* clear entire screen (only sometimes works) */
#define ANSI_SAVE()     printf("\033[s")        /* save current cursor position */
#define ANSI_RET()      printf("\033[u")        /* return to saved cursor position */
/* ANSI ESCAPE CODE MACROS END */

/* DISPLAY MACROS BEGIN */
#define FOOTER_STR "|| WELCOME TO BCOM || `!q` to quit"

#define PRINT_MSG(message)                          \
{                                                   \
    printf("\r");                                   \
    print_time(message.timestamp);                  \
    printf(" ");                                    \
    printf("\033[1;3%dm%s\033[0m: %s\033[1B\r",    \
        message.user.color,                         \
        message.user.name,                          \
        message.msg);                               \
}

#define PRINT_USER_HANDLE(user) printf("\r(\033[1;3%dm%s\033[0m): ", user->color, user->name)

#define PRINT_FOOTER(footer_str)        \
{                                       \
    print_border();                     \
    ANSI_DOWN(1);                       \
    printf(footer_str);                 \
    ANSI_DOWN(1);                       \
    print_border();                     \
    ANSI_DOWN(1);                       \
}

#if 0
#define PUT_ERR_MSG(user, msg)              \
{                                           \
    insert_to_buffer(                       \
            (struct message_t){             \
                1,                          \
                (struct user_t){user, 7},   \
                msg,                        \
                1,                          \
                time(NULL)                  \
            }                               \
    );                                      \
}
#endif

#ifndef NDEBUG
#define UI_LPRINTF(user_str, format, ...)               \
{                                                       \
    struct message_t msg;                               \
    msg.len = sprintf(msg.msg, format, ##__VA_ARGS__);  \
    msg.status = 1;                                     \
    msg.user = (struct user_t){{user_str}, 7};          \
    msg.timestamp = time(NULL);                         \
    insert_to_buffer(msg);                              \
}
#else
#define UI_LPRINTF(user_str, format, ...) {}
#endif




/* DISPLAY MACROS END */

/* GLOBAL VALUES BEGIN */
extern volatile int16_t g_cursorLeftOffset;
extern volatile int16_t g_cursorUp;
/* GLOBAL VALUES END */

/* ui_diplay.c prototypes */
void clear_display(int32_t lines);
void insert_to_buffer(struct message_t msg);
struct message_t *get_msg_buf(void);
void *start_ui(struct msg_pipe_t *pipe);
void init_client(void);
struct user_t* get_client_user(void);


#endif
