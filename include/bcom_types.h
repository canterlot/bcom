 /*
 * Title:   BCOM Project Types
 *
 * AUTHOR: Gustav O, Vitalijs K
 */

#ifndef _BCOM_TYPES_
#define _BCOM_TYPES_

#include <stdio.h>
#include <stdint.h>
#include <time.h>

/* NETWORKING MACROS BEGIN */
#define BCOM_PORT 1486U
#define BCOM_MAX_CON 1U    /* max connections per server */
#define BCOM_PREAMBLE   0x0AFE  /* preamble */
/* NETWORKING MACROS END */


/* RETURN FLAGS BEGIN */
#define BCOM_OK 0
#define BCOM_CREATE_FAIL -1
#define BCOM_SOCK_FAIL -2

#define BCOM_MSG_SET        1
#define BCOM_MSG_EMPTY      0
#define BCOM_QUIT           -1  /* redefine as enums instead */
#define BCOM_MSG_UNSET      -2
#define BCOM_MSG_WAITING    -3

#define BCOM_ENCODE_FAIL    -1
#define BCOM_ENCODE_SUCCESS  0
/* RETURN FLAGS END */

/* ARRAY LENGTHS BEGIN */
#define BCOM_BYTES_PER_COM    512U /* How many bytes get sent per transition  */
#define BCOM_MAX_MSG_BUF      100U /* Max messages in a buffer to display     */
#define BCOM_MIN_USERNAME_LEN 3U   /* Min ASCII characters in a username      */
#define BCOM_MAX_USERNAME_LEN 24U  /* Max ASCII characters in a username      */
#define BCOM_MAX_MSG_SIZE     255U /* Max bytes in a single message text body */
#define BCOM_MAX_CHAR_BYTES   3U   /* Max bytes per character                 */
/* ARRAY LENGTHS END */

/* STRUCT MACROS BEGIN*/
/* empty message struct */
#define EMPTY_MSG(x)            \
    (struct message_t)          \
    {                           \
        x,                      \
        (EMPTY_USER()),         \
        "",                     \
        0,                      \
        1,                      \
    }

/* empty user struct */
#define EMPTY_USER() (struct user_t){"", 7}
                                                                
#define MSG_FROM_USR(user, msg_str, len) (struct message_t){1, user, msg_str, len, 1}
/* STRUCT MACROS END */


/* TYPE DEFINITIONS BEGIN */
/* User object
 *  name -- username,
 * 
 *  color -- assigned color to the user
 *      0 -- black
 *      1 -- red
 *      2 -- green
 *      3 -- yellow
 *      4 -- blue
 *      5 -- magenta
 *      6 -- cyan
 *      7 -- white
 */
struct user_t
{
#if 0   /* Later there should be a unique id for every user */
    const uint32_t id; 
#endif
    char name[BCOM_MAX_USERNAME_LEN];
    uint8_t color;
};


/* Message object
 *  status:
 *      -1  -- exit program
 *      0   -- message unset
 *      1   -- message set
 * 
 *  user  -- author of the message,
 * 
 *  msg     -- message string
 *
 *  len     -- amount of characters i.e. not bytes
 *
 *  timestamp   -- seconds since the epoch 1970
 */
struct message_t
{
    int8_t status;
    struct user_t user;
    char msg[BCOM_MAX_MSG_SIZE];
    uint32_t len; /* len in characters (not bytes) */
    time_t timestamp;
};


/* Input buffer for displaying message while user types it */
struct input_buffer_t {
    uint16_t len;           /* message length in characters                 */
    uint16_t size;          /* message length in bytes                      */
    struct ln_msg_t *msg;   /* linked list of individual characters of the  *
                             * message, for easier insertion/deletion       */
};

/* Double-Linked list of characters for the input buffer for easy
 * insertion and removal */
struct ln_msg_t {
    char ch[BCOM_MAX_CHAR_BYTES];
    struct ln_msg_t *prev;
    struct ln_msg_t *next;;
};

struct msg_pipe_t
{
    struct message_t in;
    struct message_t out;
};

#if 0
/* Message moved to terminal display
 * TODO: We need more?
 * 
 * from VK: redefined in struct message_t
 */
struct bcom_data_t {
    char bcom_namn[24];
    char bcom_data[256];
};

/* incomming Message structure */
struct bcom_buffer_t {
    char *ip_address;
    struct bcom_data_t user_data;
};
#endif

/* TYPE DEFINITIONS END */


#endif /* _BCOM_TYPES_ */
