#ifndef _BCOM_USER_INPUT_H_
#define _BCOM_USER_INPUT_H_

#include "bcom_types.h"

/* user_input.c prototypes */
struct input_buffer_t *get_input_buffer();
void input_buffer_clear();
int32_t lnlist_arr(struct input_buffer_t *input_buffer, char* msgarr);
void* get_user_input(struct message_t* message);
int8_t get_username(struct user_t *user);

#endif
