/*
 * TITLE: BCOM Client
 * 
 * DESCRIPTION: Client side of BCOM.
 * AUTHOR: Vitalijs K
 *
 * priority guidelines:
 *      very high   -- feature is critically necessary for functionality of
 *                      software e.g. fixing segfaults, proper networking, having ui
 *
 *      high        -- feature is thought to be core functionality without which
 *                      software is incomplete e.g. adding unique user ID's,
 *                      encryption
 *
 *      medium      -- feature would be of great convenience to the user
 *                      e.g. protected names, @'ing people, etc
 *
 *      low         -- feature would be pleasant to have for the user
 *                      e.g. user colors, timestamps
 *     
 *      very low    -- feature brings some polish to current system
 * 
 * This TODO list for the project in general. Some files have their own list.
 * TODO (priority): 
 *      o Make networking work via: (very high)
 *          - centralized server-client
 *          - decentralized, peer-to-peer, CAN bus like communication
 *
 *      o Unify with init and server (very high)
 *
 *      o Add encryption    (high)
 *          - this must be a core functionality later
 *          - but focus on getting networking to work first
 *
 *      o Create a (linked) list of all users connected to server (high)
 *
 *      o Add unique IDs to user object (high)
 *
 *      o Cursor is currently using global variable to get cursor position (low)
 *          redo using different technique
 *          - same with scroll, add mutex at least
 *          - it works so whatever ¯\_(`-`)_/¯
 *
 *      o Save last buffer to text file (low)
 *          - encrypt text file?
 *
 *      o Add @'ing highlighting if user's name is mentioned (low)
 *
 *      o Add 256 colors instead of 8 colors (very low)
 *
 *      o Remember last username (very low)
 *
 *      o and much more
 */

/* SYSTEM INCLUDES */
#include <pthread.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/select.h>

/* LOCAL INCLUDES */
#include "bcom_types.h"
#include "client.h"
#include "ui.h"
#include "aes.h"

#define NEW_CODE 0

#define CLIENT_STR "@client"

extern pthread_mutex_t msg_pipe_mtx;
extern char ip_address[16];
/*******************************************************************************
 * PRIVATE FUNCTIONS                                                           *
 ******************************************************************************/

#if NEW_CODE

static void *
client_recv(void **args)
{
    uint8_t data[BCOM_BYTES_PER_COM];
    struct message_t *msgin;
    int sockfd;
    int res;
    struct timespec ts; /* timer */

    ts.tv_sec = 0;
    ts.tv_nsec = 20000000; /* 20 ms delay */

    msgin = args[1];
    sockfd = *(int*)args[0];

    while (1)
    {
        res = recv(sockfd, data, BCOM_BYTES_PER_COM, 0);
        //UI_LPRINTF(CLIENT_STR, "Received %d bytes", res);
        if (res > 0)
        {
            //UI_LPRINTF(CLIENT_STR, "Message received back, SUCCESS!");
            deserialize(msgin, data);
            *msgin = EMPTY_MSG(BCOM_MSG_UNSET);
        }
        nanosleep(&ts, NULL);
    }
    return NULL;
}

static void *
client_send(int *sockfd, fd_set *master, fd_set *read_fd,
        struct message_t *msgout)
{
    uint8_t data[BCOM_BYTES_PER_COM];
    struct timespec ts; /* timer */
    int res;

    ts.tv_sec = 0;
    ts.tv_nsec = 20000000; /* 20 ms delay */

    while (1)
    {
        *read_fd = *master;
        if (select((*sockfd) + 1, read_fd, NULL, NULL, NULL) == -1)
        {
            UI_LPRINTF(CLIENT_STR, "Select error");
        }

        if (FD_ISSET(*sockfd, read_fd))
        {
            if (msgout->status == BCOM_MSG_SET)
            {
                serialize(msgout, data);
                res = send(*sockfd, data, BCOM_BYTES_PER_COM, MSG_NOSIGNAL);
                UI_LPRINTF(CLIENT_STR, "Sent %d bytes", res);
                if (res > 0)
                {
                    UI_LPRINTF(CLIENT_STR, "Message sent from user to server");
                    msgout->status = BCOM_MSG_UNSET;
                }
            }
        }
        nanosleep(&ts, NULL);
    }
    return NULL;
}


#else

static void
send_recv(int i, int sockfd, struct msg_pipe_t *pipe)
{
    uint8_t send_buf[BCOM_BYTES_PER_COM];
    uint8_t recv_buf[BCOM_BYTES_PER_COM];
    int res;

    if (i == 0)
    {
        if (pipe->out.status == BCOM_MSG_SET)
        {
            serialize(&pipe->out, send_buf);
            send(sockfd, send_buf, BCOM_BYTES_PER_COM, MSG_NOSIGNAL);
            send(sockfd, NULL, 1, MSG_NOSIGNAL);
            //UI_LPRINTF(CLIENT_STR, "Sent %d bytes", res);
            //UI_LPRINTF(CLIENT_STR, "Message sent from user to server");
            pipe->out.status = BCOM_MSG_UNSET;
        }
    }
    else
    {
        res = recv(sockfd, recv_buf, BCOM_BYTES_PER_COM, 0);
        if (res > 0)
        {
            //UI_LPRINTF(CLIENT_STR, "Received %d bytes", res);
            //UI_LPRINTF(CLIENT_STR, "Message received back, SUCCESS!");
            deserialize(&pipe->in, recv_buf);
            insert_to_buffer(pipe->in);
            pipe->in = EMPTY_MSG(BCOM_MSG_UNSET);
        }
    }
}

#endif

static void
connect_request(int *sockfd, struct sockaddr_in *server_addr)
{
    const struct timespec ts = {0, 500000000}; /* 20 ms timer */
    while ((*sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        UI_LPRINTF(CLIENT_STR, "Socket error");
        nanosleep(&ts, NULL);
    }
    server_addr->sin_family = AF_INET;
    server_addr->sin_port = htons(BCOM_PORT);
    server_addr->sin_addr.s_addr = inet_addr(ip_address);
    memset(server_addr->sin_zero, '\0', sizeof(server_addr->sin_zero));

    while (connect(*sockfd, (struct sockaddr *)server_addr, 
                sizeof(struct sockaddr)) == -1)
    {
        UI_LPRINTF(CLIENT_STR, "Connect error");
        nanosleep(&ts, NULL);
    }
}

/*******************************************************************************
 * PUBLIC FUNCTIONS                                                            *
 ******************************************************************************/

#if NEW_CODE

void *
bcom_client(struct msg_pipe_t *pipe)
{
    int sockfd, fdmax;
    struct sockaddr_in server_addr;
    fd_set master;
    fd_set read_fd;

    pthread_t client_recv_th;
    void *client_recv_args[2];

    //pthread_t client_recv_th;
    //void *client_recv_args[3];

    connect_request(&sockfd, &server_addr);
    FD_ZERO(&master);
    FD_ZERO(&read_fd);
    FD_SET(0, &master);
    FD_SET(sockfd, &master);

    fdmax = sockfd;

    client_recv_args[0] = &sockfd;
    client_recv_args[1] = &pipe->in;


    pthread_create(&client_recv_th, NULL,
            (void *(*)(void*))client_recv,
            client_recv_args);

    client_send(&fdmax, &master, &read_fd, &pipe->out);

    return NULL;
}

#else

void *
bcom_client(struct msg_pipe_t *pipe)
{
    int sockfd, fdmax, i;
    struct sockaddr_in server_addr;
    fd_set master;
    fd_set read_fd;

    connect_request(&sockfd, &server_addr);
    FD_ZERO(&master);
    FD_ZERO(&read_fd);
    FD_SET(0, &master);
    FD_SET(sockfd, &master);

    fdmax = sockfd;

    while (1)
    {
        read_fd = master;
        if (select(fdmax + 1, &read_fd, NULL, NULL, NULL) == -1)
        {
            UI_LPRINTF(CLIENT_STR, "Select error");
        }

        for (i = 0; i <= fdmax; i++)
        {
            if (FD_ISSET(i, &read_fd))
            {
                send_recv(i, sockfd, pipe);
            }
        }
    }
    return NULL;
}
#endif
