/*
 * Title:   Initialization code
 * Author:  Author
 * 
 * Verison: 0.1
 * TODO: ADD
 *
 * Note:
 */

#if 0

/* System Headers */
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>

#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <ui.h>
#include <client.h>
#include <bcom_types.h>

#define MAX 80 
#define SA struct sockaddr 
/* Project Headers */
//#include <thread.h>

#define BCOM_DEBUG 1
#include "bcom_debug.h"

/* Variables */
#define PORT 1489
#define ADDR "127.0.0.1" // Default

#define BUFMAX 1024


enum CON_TYPE{
    SERVER = 0,
    CLIENT
};

struct bcom_conaddr {
    enum CON_TYPE type;
    struct sockaddr_in socket_addr;
};

/* Start */

void bcom_connection(int fd, struct bcom_conaddr *connection)
{
    int status = 0;

    /* Perform Connection test */
    status = connect(
            fd, 
            (struct sockaddr *) &connection->socket_addr, 
            sizeof(connection->socket_addr));
    if (status == -1 ) {
        printf("No connections found, setting up server\n");
        connection->type = 0;
    }

    connection->type = 1;
    return;
}

void *bcom_server()
{
    DBG("SERVER started\n");
    fflush(stdout);

    char buff[BUFMAX]; 
    int n; 
    
    int sockfd, connfd, len; 
    struct sockaddr_in servaddr, cli; 
  
    // socket create and verification 
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        printf("socket creation failed...\n"); 
        exit(0); 
    } 
    else
        printf("Socket successfully created..\n"); 
    bzero(&servaddr, sizeof(servaddr)); 
 
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
    servaddr.sin_port = htons(PORT); 
  
    // Binding newly created socket to given IP and verification 
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
        printf("socket bind failed...\n"); 
        exit(0); 
    } 
    else
        printf("Socket successfully binded..\n"); 
    
    // Now server is ready to listen and verification 
    if ((listen(sockfd, 5)) != 0) { 
        printf("Listen failed...\n"); 
        exit(0); 
    } 
    else
        printf("Server listening..\n"); 
    len = sizeof(cli); 
  
    // Accept the data packet from client and verification 
    connfd = accept(sockfd, (SA*)&cli, &len); 
    if (connfd < 0) { 
        printf("server acccept failed...\n"); 
        exit(0); 
    } 
    else
        printf("server acccept the client...\n"); 
 


    while(1) {
        bzero(buff, MAX); 
  
        // read the message from client and copy it in buffer 
        read(connfd, buff, sizeof(buff)); 
        
        // print buffer which contains the client contents 
        printf("From client: %s\t to client: ", buff); 
        
        bzero(buff, MAX); 
        n = 0; 
        // copy server message in the buffer 
        while ((buff[n++] = getchar()) != '\n') 
            ; 
  
        // and send that buffer to client 
        write(connfd, buff, sizeof(buff)); 
  
        // if msg contains "Exit" then server exit and chat ended. 
        if (strncmp("exit", buff, 4) == 0) { 
            printf("Server Exit...\n"); 
            break; 
        } 
        sleep(1); 
    }
    DBG("Task Ended");
    return NULL;
}

void *bcom_client_w()
{
    DGB("CLIENT W started\n");
    int status;
    char buf[200];

    /* Main while loop */
    while (1) {
        
        if () {

        }

    }
    pthread_exit(0);
}

void *bcom_client()
{
    DBG("CLIENT started\n");
    int sockfd; 
    struct sockaddr_in servaddr, cli; 
    char buff[BUFMAX]; 
    int n; 
  
    // socket create and varification 
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        printf("socket creation failed...\n"); 
        exit(0);
    } 
    else
        printf("Socket successfully created..\n"); 
    bzero(&servaddr, sizeof(servaddr)); 
  
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaLost Frequenciesddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    servaddr.sin_port = htons(PORT); 
  
    // connect the client socket to server socket 
    if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) { 
        printf("connection with the server failed...\n"); 
        exit(0); 
    } 
    else
        printf("connected to the server..\n"); 
  
    /* UI CODE BEGINS */
    /* to Sparcy: only edit the while loop */
    signal(SIGINT, handler); /* init SIGINT handler */
    
    /* timer settings */
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = 50000000; /* update screen every 50 ms */

    while (1)   /* get client user name */
    {
        if (init_client_user() == 1) /* initiates user client by taking username from user input */
            break;
        clear_display(-1); /* clear all terminal lines visible */
        printf("Invalid username. Must be between 3-24 characters long.\n");
        printf("Allowed symbols: a-z,A-Z,0-9,_).\n");
    }

    tty_set_raw(); /* set terminal into raw mode 
                    * (important that you do not print 
                    * anything after this has run
                    * unless you exit raw mode with tty_reset()
                    * exiting raw mode breaks display, 
                    * so program has to stop running before that*/

    struct message_t msg_in; /* received message from server to display for user */
    struct message_t msg_out;   /* message from user to send to server*/

    /* start user input acquisition thread */
    pthread_t get_user_input_thread;
    pthread_create(&get_user_input_thread,
            NULL,
            (void* (*)(void*))get_user_input,
            &msg_out);

    update_display(); /* updates display once */
    while (1)
    {
        switch (msg_out.status)
        {
            case BCOM_MSG_UNSET: /* if message is unset, start waiting for it (can be skipped)*/
                msg_out.status = BCOM_MSG_WAITING;
                break;

            case BCOM_QUIT: /* if user types `!q` */
                clear_display(-1);  /* clear display (-1 to clear all lines from the top of the termianl)*/
                free_all();     /* free all mallocs used by UI*/
                tty_reset();    /* returns terminal into canonical mode
                                 * (important to do this before exiting)*/
                _exit(0);       /* actually it should return to main
                                 * thread and exit there
                                 * so all child threads are killed,
                                 * otherwise it will only kill
                                 * client thread and this will break 
                                 * the application if there are
                                 * still parent threads running */
                break;

            case BCOM_MSG_EMPTY: /* if message typed by user is empty
                                  * (user presses enter without
                                  * typing anything), do not send it,
                                  * just set it to unset and wait
                                  * for input again */
                msg_out.status = BCOM_MSG_UNSET;
                break;

            case BCOM_MSG_SET: /* this is where you should send
                                * it to the server */
                msg_out = EMPTY_MSG(BCOM_MSG_UNSET);
                break;

            default:
                break;
        }

        switch (msg_in.status)
        {
            case BCOM_MSG_WAITING:
                break;

            case BCOM_MSG_SET: /* you should pass messages coming
                                * from server into insert_to_buffer here */
                 /* this inserts message into display message buffer
                  * so message gets printed on next update_display() */
                insert_to_buffer(msg_in);  /* takes struct message_t pointer*/
                msg_in.status = BCOM_MSG_UNSET;
                break;
            default:
                break;
        }
        update_display();  /* update display */
        nanosleep(&ts, NULL); /* sleep for 50 ms */
    } 
    /* UI CODE ENDS */

    /* Client Write task */
    pthread_t bcom_cli_write;
    pthread_create(&bcom_cli_write,
            NULL,
            )

    while (1) {
        bzero(buff, sizeof(buff)); 
        printf("Enter the string : "); 
        n = 0; 
        while ((buff[n++] = getchar()) != '\n') 
            ; 
        write(sockfd, buff, sizeof(buff)); 
        bzero(buff, sizeof(buff)); 
        read(sockfd, buff, sizeof(buff)); 
        printf("From Server : %s", buff); 
        if ((strncmp(buff, "exit", 4)) == 0) { 
            printf("Client Exit...\n"); 
            break; 
        } 
    }
    
    DBG("Task Ended");
    pthread_exit(NULL);
}


int main(int argc, char* argv[])
{
    printf("*** Application Start ***\n");
    int status = 0;
    char mode;
    if (argc >= 2) {
        mode = argv[1][0];
    }
    else
        mode = 0;

#if 0
    int sockfd, n;
    struct bcom_conaddr server_ip;
    pthread_t bcom_, bcom_main; 

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Socket not created\n");
        exit(0); 
    }

    server_ip.type = 0;
    server_ip.socket_addr.sin_family = AF_INET;
    server_ip.socket_addr.sin_port = htons(1489);
    server_ip.socket_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
   
    /* Check settings */
    bcom_connection(sockfd, &server_ip);
#endif

    /* Setup Task*/
    if (mode == 's'){
        pthread_t server_id;



        status = pthread_create(&server_id, NULL, bcom_server, NULL); 
        if (status != 0){
            printf("Error creating thread[%d]", status);
            abort();
        }
    }

    else if (mode == 'c') {
        pthread_t client_id;
        status = pthread_create(&client_id, NULL, bcom_client, NULL);
        if (status != 0){
            printf("Error creating thread[%d]", status);
            abort();
        }
    }

    else {
        printf(" Unsupported mode\n");
    }

    pthread_exit(NULL);
}
#endif
