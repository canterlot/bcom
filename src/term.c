/*
 * TITLE: Terminal Configurations
 * 
 * DESCRIPTION: Configures terminal into raw (non-canonical) mode
 *              and reconfigured back into origin settings on exit.
 * AUTHOR: Vitalijs K
 */

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

#include "ui.h"
#include "client.h"
#include "term.h"
#include "user_input.h"



/*****************************************************************************
 *                         PRIVATE FUNCTIONS 
 *****************************************************************************/

/* Save previous termios config */
static struct termios*
prev_term()
{
    static struct termios* prev_term;
    if (prev_term == NULL)
    {
        prev_term = (struct termios*)malloc(sizeof(struct termios));
        tcgetattr(STDIN_FILENO, prev_term); 
    }
    return prev_term;
}

/*****************************************************************************
 *                         PUBLIC FUNCTIONS 
 *****************************************************************************/


/* SINGINT handler: restore tty settings and exit */
void
handler(int32_t sig)
{
    clear_display(-1);
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, prev_term()) == -1)
    {
        printf("Failed to return to canonical mode.\n");
    }
    free_all();
    printf("Interrupt signal triggered.\n");
    _exit(0);
}

/* frees all mallocs used by UI */
void free_all()
{
    /* free linked list input buffer */
    input_buffer_clear();
    free(get_input_buffer()->msg);
    free(get_input_buffer());
    free(get_msg_buf());
    free(get_client_user());
}

/* return to original terminal settings */
void
tty_reset()
{
    tcsetattr(STDIN_FILENO, TCSAFLUSH, prev_term());
}

/* Enter non-canonical mode */
int32_t
tty_set_raw()
{
    prev_term();
    struct termios t;
    tcgetattr(STDIN_FILENO, &t);

    /* set cbreak flags */
    t.c_lflag &= ~(ICANON | ECHO | ECHOE); /* disable canonical mode */
    t.c_lflag |= ISIG;
    t.c_iflag &= ~(BRKINT | ICRNL | IGNBRK | IGNCR);
    t.c_oflag &= ~OPOST;

    t.c_cc[VMIN] = 1; /* char-at-a-time input */

    /* enable blocking (runs on it's own thread so it's no problem)*/
    t.c_cc[VTIME] = 0;

    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &t) == -1)
    {
        return -1;
    }

    return 0;
}

