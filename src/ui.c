/*
 * TITLE: UI Display
 * 
 * DESCRIPTION: Terminal based user_t interface which functions
 * via means of ANSI escape code
 * 
 * AUTHOR: Vitalijs K
 * 
 * TODO: 
 *      o Fix small bug where when typing message which 2 lines (low)
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include "../include/term.h"

#include "ui.h"
#include "client.h"
#include "user_input.h"

pthread_mutex_t disp_mtx = PTHREAD_MUTEX_INITIALIZER;

/******************************************************************************
 * PRIVATE FUNCTIONS                                                          *
 ******************************************************************************/



/* initiate a instance of client user
 * returns -1 if failed */
static uint32_t
init_client_user()
{
    struct user_t* client_user = (struct user_t*) get_client_user();
    if (!client_user)
    {
        return -1;
    }
    if (get_username(client_user) != 1) /* set username from user input */
    {
        return -1;
    }

    srand(time(0)); /* randomize seed */
    uint8_t color = (rand() % (1 - 6 + 1)) + 1; /* get random color*/
    client_user->color = color;
    return 0;
}


/* Prints a line across the terminal `=====...'*/
static void
print_border()
{                        
    int32_t counter;
    const struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    printf("\r");
    for (counter = 0; counter < w.ws_col; counter++)
    {
        printf("=");
    }
    printf("\r");
}

/* places cursor where it is supposed to be */
static void
cursor(int32_t msg_pos)
{
    const struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);   /* get terminal dimensions */
    
    const uint32_t filler_offset = (uint32_t)strlen(get_client_user()->name)
                             + FILLER_COLS_IN - 1;
    const uint32_t lines = (msg_pos + filler_offset)
                        / (uint32_t)w.ws_col;
    
    ANSI_RESET();
    ANSI_DOWN(w.ws_row - FOOTER_ROWS + 4);

    if (lines == 0)
    {
        ANSI_RIGHT(filler_offset + msg_pos);
    }
    else
    {
        ANSI_DOWN(lines);
        ANSI_RIGHT((msg_pos + filler_offset) % w.ws_col);
    }
}

/* formats timestamp into hour:minute:second format and prints */
static void
print_time(time_t timestamp)
{
    struct tm lt; /* local time */
    localtime_r((const time_t*)&timestamp, &lt);
    printf("%02d:%02d:%02d", lt.tm_hour, lt.tm_min, lt.tm_sec);
}

#if 0 /* TODO remake display with an array instead. */
static void
canvas()
{
    

}
#endif

/* Print message on stdout,
 * uses ANSI escape characters */
static void
update_display()
{
    /* get pointer to heap buffer of past messages*/
    const struct message_t* msg = get_msg_buf();  
    int32_t counter;
    int32_t valid_msg_count = 0; /* amount of messages in buffer */

    const struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);   /* get terminal dimensions */

    /* find out how many messages line there are 
     * and how many empty lines to print */
    int32_t msg_lines_sum = 0;
    for (counter = 0; counter < BCOM_MAX_MSG_BUF; counter++)
    {
        if (msg[counter].status == 1)
        {
            msg_lines_sum += 1 + (((uint32_t)strlen(msg[counter].user.name)
                        + msg[counter].len + FILLER_COLS))
                        / (uint32_t)w.ws_col;
            valid_msg_count++;
        }
    }
    clear_display(-1);
    print_border();
    ANSI_DOWN(1);

    /* how many tailing lines of messages not to print */
    int32_t lines_nprint = 0;
    if (msg_lines_sum + FILLER_ROWS < w.ws_row)
    {
        g_cursorUp = 0;
        ANSI_DOWN(w.ws_row - msg_lines_sum - FILLER_ROWS);
    }
    else
    {
        lines_nprint = (msg_lines_sum + FILLER_ROWS) - w.ws_row;
        /* do not let scroll go over boundaries */
        if (g_cursorUp > valid_msg_count - w.ws_row + FILLER_ROWS)
            g_cursorUp = valid_msg_count - w.ws_row + FILLER_ROWS;
    }

    int32_t cursorUp = g_cursorUp; /* how many times user scrolled up */
    /* Print out buffer of past messages */
    int32_t msg_lines;
    int32_t counter_os;
    for (counter = BCOM_MAX_MSG_BUF - 1; counter >= 0; counter--)
    {
        counter_os = counter + cursorUp; /* offset messages */
        if (counter_os > BCOM_MAX_MSG_BUF)
            counter_os = BCOM_MAX_MSG_BUF;

        if (msg[counter_os].status == 1)
        {
            if (lines_nprint - cursorUp > 0)
            {
                /* how many lines does a message take up */
                msg_lines = 1 + (((uint32_t)strlen(msg[counter_os].user.name)
                            + msg[counter_os].len + FILLER_ROWS)
                            / (uint32_t)w.ws_col);
                lines_nprint -= msg_lines;
            }
            else if (lines_nprint < 0)
            {
                ANSI_DOWN(-lines_nprint - 1);
                lines_nprint = 0;
                PRINT_MSG(msg[counter_os]);
            }
            else
            {
                PRINT_MSG(msg[counter_os]);
            }
        }
    }

    ANSI_ROW(w.ws_row - FOOTER_ROWS);
    PRINT_FOOTER(FOOTER_STR);
    struct input_buffer_t* input_buffer = get_input_buffer();
    struct ln_msg_t* msg_ch = input_buffer->msg;

    printf("Bytes: [%d/%d]", input_buffer->size, BCOM_MAX_MSG_SIZE);
    ANSI_DOWN(1);
    PRINT_USER_HANDLE(get_client_user());

    ANSI_SAVE();
    for (counter = 0; msg_ch->next ;counter++)
    {
        printf("%s", msg_ch->ch);
        msg_ch = msg_ch->next;
    }
    cursor(input_buffer->len - g_cursorLeftOffset);
    fflush(stdout);
}
/******************************************************************************
 * PUBLIC FUNCTIONS                                                           *
 ******************************************************************************/
void
init_client(void)
{
    while (1)   /* get client user name */
    {
        if (init_client_user() == 0)
            break;
        clear_display(-1);
        printf("Invalid username. Must be between 3-24 characters long.\n");
        printf("Allowed symbols: a-z,A-Z,0-9,_).\n");
    }
}

/* get pointer to user on heap memory 
 * assign as const for safety */
struct user_t*
get_client_user()
{
    static struct user_t* client_user = NULL;
    if (!client_user)
    {
        client_user = (struct user_t*) malloc(sizeof(struct user_t));
        *client_user = EMPTY_USER();   /* don't leave uninitiated */
    }
    return client_user;
}


/* Return a pointer to the heap where message buffer lies,
 * initiates memory if it has not been initiated yet. */
struct message_t*
get_msg_buf()
{
    static struct message_t* msg_buffer = NULL;
    uint32_t counter;
    if (!msg_buffer)
    {
        msg_buffer = (struct message_t*) calloc(BCOM_MAX_MSG_BUF,
                sizeof(struct message_t));
        for (counter = 0; counter < BCOM_MAX_MSG_BUF; counter++)
        {
            msg_buffer[counter] = EMPTY_MSG(BCOM_MSG_EMPTY);
        }
    }
    return msg_buffer;
}

/* Insert a message into buffer to be printed
 *  msg     --  insert this message */
void
insert_to_buffer(struct message_t msg)
{
    struct message_t* msg_buffer = get_msg_buf();
    uint32_t counter;
    /* Move messages in the buffer, throw out the last one */
    pthread_mutex_lock(&disp_mtx);
    for (counter = (BCOM_MAX_MSG_BUF - 1); counter > 0; counter--)
    {
        msg_buffer[counter] = msg_buffer[counter - 1];
    }
    pthread_mutex_unlock(&disp_mtx);
    msg_buffer[0] = msg;
}

/* Clears previously printer lines using ANSI escape code 
 *
 * removes up-to `lines` from the top. -1 clears everything */
void
clear_display(int32_t lines)
{
    if (lines == 0) {return;}
    int32_t counter;
    const struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);   /* get terminal dimensions */

    ANSI_RESET();
    ANSI_CLEARLN();

    for (counter = 1; counter < w.ws_row; counter++)
    {
        if (lines > -1 && counter - 1 >= lines)
        {
            break;
        }
        ANSI_DOWN(1);    /* Move cursor down */
        ANSI_CLEARLN();  /* Clear line */
    }
    ANSI_RESET();
}


void*
start_ui(struct msg_pipe_t *pipe)
{
    /* timer settings */
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = 50000000; /* 50 ms */

    tty_set_raw(); /* set raw mode */

    /* start user input acquisition thread */
    struct message_t msg_out;
    msg_out = EMPTY_MSG(BCOM_MSG_WAITING);
    pthread_t get_user_input_thread;
    pthread_create(&get_user_input_thread,
            NULL,
            (void* (*)(void*))get_user_input,
            &msg_out);

    update_display();
    while (1)
    {
        switch (msg_out.status)
        {
            /* if no message is typed yet */
            case BCOM_MSG_UNSET:
                msg_out.status = BCOM_MSG_WAITING;
                break;

            /* if quitting */
            case BCOM_QUIT:
                clear_display(-1);
                free_all();
                tty_reset();
                exit(0);

            /* if message typed is empty */
            case BCOM_MSG_EMPTY:
                msg_out.status = BCOM_MSG_UNSET;
                break;

            /* if message is set */
            case BCOM_MSG_SET:
                msg_out.timestamp = time(NULL);
                memcpy(&pipe->out, &msg_out, sizeof(struct message_t));
                msg_out = EMPTY_MSG(BCOM_MSG_UNSET);
                break;

            default:
                break;
        }

        if (pipe->in.status == BCOM_MSG_SET)
        {
            insert_to_buffer(pipe->in);
            pipe->in = EMPTY_MSG(BCOM_MSG_UNSET);
            //pipe->in.status = BCOM_MSG_UNSET;
        }
        update_display();
        ANSI_RET();
        nanosleep(&ts, NULL);
    }
    printf("Program unexpectedly quit.\n");
    return NULL;
}
