#include <pthread.h>
#include <signal.h>


#include "client.h"
#include "server.h"
#include "ui.h"
#include "term.h"




//pthread_mutex_t msg_pipe_mtx = PTHREAD_MUTEX_INITIALIZER;
char ip_address[16] = BCOM_LOCAL_IP;

int main(int argc, char* argv[])
{
    char mode;
    if (argc >= 2) {
        mode = argv[1][0];
    }
    else
        mode = 0;

    signal(SIGINT, handler); /* init SIGINT */
    
    /* pipe.in is received message from server to display for user */
    /* pipe.out is message from user to send to server*/
    struct msg_pipe_t pipe;
    pipe.in = EMPTY_MSG(BCOM_MSG_UNSET);
    pipe.out = EMPTY_MSG(BCOM_MSG_UNSET);

    init_client();

    /* create separate thread for UI */
    pthread_t ui_thread;
    pthread_create(&ui_thread,
            NULL,
            (void* (*)(void*))start_ui,
            &pipe);

    if (mode == 's')
    {
        /* create separate thread for server */
        pthread_t server;
        pthread_create(&server,
                NULL,
                bcom_server,
                NULL);
    }
    /* main becomes the client thread */
    if (argc >= 3)
    {
        strncpy(ip_address, argv[2], 15);
    }
    bcom_client(&pipe);
    return 0;
}
