/*
** TITLE: Advanced Encryption Algorithm
**
** DESCRIPTION: Will include message encryption tools
** AUTHOR:
**
** TODO:
**      o Figure out asymmetric key generation
**
**      o Add encryption
*/

#include "bcom_types.h"
#include <stdlib.h>
#include <string.h>

#define TESTING 1

#if TESTING

int8_t 
serialize(struct message_t *message, uint8_t *data)
{
    memcpy(data, message, sizeof(struct message_t));
    return EXIT_SUCCESS;
}

int8_t 
deserialize(struct message_t *message, uint8_t *data)
{
    memcpy(message, data, sizeof(struct message_t));
    return EXIT_SUCCESS;
}
#else

#if 0
#define _WITH_PREAMBLE
#endif
/* converts message_t type to raw bytes to be sent
 * 
 * this is where encrypting will take place 
 *
 *  `data` is a bytes array of size 1024
 *  `data` structure is defined as:
 *   +-----------+----------+---------------------------------------+
 *   |  BYTE#    |  BYTES   |   DATA DESCRIPTION                    |
 *   +-----------+----------+---------------------------------------+
 *   |  0-24     |   25     |  Username                             |
 *   |  25       |   1      |  User color                           |
 *   |  26-29    |   4      |  User Unique ID                       |
 *   |  30-37    |   8      |  Timestamp                            |
 *   |  38-41    |   4      |  Message length                       |
 *   |  42-297   |   255    |  Message                              |
 *   |  298      |   1      |  Padding null                         |
 *   |  REST     |   ---    |  Leave random for better encryption   |
 *   |           |          |                                       |
 *   +-----------+----------+---------------------------------------+
 *
 *  More changes will come
 *  (might not need so many padding nulls tbh)
 * */ 
int8_t 
serialize(struct message_t *message, uint8_t *data)
{
    uint32_t byte_nr = 0;

#ifdef _WITH_PREAMBLE
    /* preamble */
    data[byte_nr] = (uint8_t)(BCOM_PREAMBLE >> 8);
    byte_nr++;
    data[byte_nr] = (uint8_t)(BCOM_PREAMBLE & 0xFF);
    byte_nr++;
#endif

    /* user name*/
    memcpy((void*)&data[byte_nr],
            (void*)message->user.name,
            BCOM_MAX_USERNAME_LEN);
    byte_nr += BCOM_MAX_USERNAME_LEN;

#if 0
    /* padding null */
    memset(&data[byte_nr], 0, 1);
    byte_nr += 1;
#endif

    /* user color */
    memcpy((void*)&data[byte_nr], (void*)&(message->user.color), 1);
    byte_nr += 1;
    
    /* user id */
    /* for now does not exists */
    byte_nr += 4;

    /* timestamp */
    memcpy((void*)&data[byte_nr], (void*)&(message->timestamp), 8);
    byte_nr += 8;

    /* message length in chars, (not bytes) */
    memcpy((void*)&data[byte_nr], (void*)&(message->len), 4);
    byte_nr += 4;

    /* message length in chars, (not bytes) */
    memcpy((void*)&data[byte_nr], (void*)message->msg, BCOM_MAX_MSG_SIZE);
    byte_nr += BCOM_MAX_MSG_SIZE;

    /* padding null */
    memset(&data[byte_nr], 0, 1);
    byte_nr += 1;

    return 0;
}

int8_t
#ifdef _WITH_PREAMBLE
deserialize(struct message_t *message, uint8_t *data, uint16_t *prem)
#else
deserialize(struct message_t *message, uint8_t *data)
#endif
{
    uint32_t byte_nr = 0;

#ifdef _WITH_PREAMBLE
    /* preamble */
    *prem = ((uint16_t)(data[byte_nr]) << 8) | (uint16_t)(data[byte_nr + 1]);
    byte_nr += 2;
#endif

    /* user name*/
    memcpy((void*)message->user.name,
            (void*)&data[byte_nr],
            BCOM_MAX_USERNAME_LEN);
    byte_nr += BCOM_MAX_USERNAME_LEN;

#if 0
    /* padding null */
    byte_nr += 1;
#endif

    /* user color */
    memcpy((void*)&(message->user.color), (void*)&data[byte_nr], 1);
    byte_nr += 1;
    
    /* user id */
    /* for now does not exists */
    byte_nr += 4;

    /* timestamp */
    memcpy((void*)&(message->timestamp), (void*)&data[byte_nr], 8);
    byte_nr += 8;

    /* message length in chars, (not bytes) */
    //memcpy((void*)&(message->len), (void*)&data[byte_nr], 4);
    message->len = ((uint32_t)(data[byte_nr]) << 24) |
                    ((uint32_t)(data[byte_nr + 1]) << 16) |
                    ((uint32_t)(data[byte_nr + 2]) << 8) |
                    (uint32_t)(data[byte_nr + 3]);
    byte_nr += 4;

    /* message length in chars, (not bytes) */
    memcpy((void*)message->msg, (void*)&data[byte_nr], BCOM_MAX_MSG_SIZE);
    byte_nr += BCOM_MAX_MSG_SIZE;

    return 0;
}
#endif
