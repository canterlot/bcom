/*
 * TITLE: User Input Functionality
 * 
 * DESCRIPTION: Handles user typing messages
 * via means of ANSI escape code
 * 
 * AUTHOR: Vitalijs K
 * 
 * TODO: 
 *      o Add more commands (medium)
 *          - !h for help
 *          - !c [args] for change color
 *          - !n <name> for change name
 *          - !l for list of users
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

#include "client.h"
#include "ui.h"

#define BCOM_DEBUG 0
#if BCOM_DEBUG
#include "bcom_debug.h"
#endif


volatile int16_t g_cursorLeftOffset = 0;
volatile int16_t g_cursorUp = 0;

/* user input buffer to display what user is typing */
struct input_buffer_t*
get_input_buffer()
{
    static struct input_buffer_t* input_buffer = NULL;
    if (!input_buffer)
    {
        input_buffer = (struct input_buffer_t*)malloc(sizeof(struct input_buffer_t));
        input_buffer->size = 1;
        input_buffer->len = 1;
        input_buffer->msg = malloc(sizeof(struct ln_msg_t));
        input_buffer->msg->ch[0] = '\0';
        input_buffer->msg->prev = NULL;
        input_buffer->msg->next = NULL;
    }
    return input_buffer;
}

/* insert char into input_buffer after msg_ch character
 *
 * return pointer to new char */
static struct ln_msg_t*
input_buffer_insert(
        struct input_buffer_t* input_buffer,
        struct ln_msg_t* msg_ch,
        char* ch)
{
    if (!msg_ch || input_buffer->size >= BCOM_MAX_MSG_SIZE - 1)
        return NULL;

    struct ln_msg_t* new_msg_ch = malloc(sizeof(struct ln_msg_t));
    strncpy(new_msg_ch->ch, ch, BCOM_MAX_CHAR_BYTES);
    /* if inserting between characters
     * (if not at beginning, will always e between some char and ending '\0')*/
    if (msg_ch->prev)
    {
        new_msg_ch->prev = msg_ch->prev;
        msg_ch->prev->next = new_msg_ch;
    }
    /* if inserting at the beginning of non-empty linked list */
    else
    {
        new_msg_ch->prev = NULL;        /* prev char does not exists */
        input_buffer->msg = new_msg_ch;
    }
    new_msg_ch->next = msg_ch;      /* link to next char */
    msg_ch->prev = new_msg_ch;      /* relink prev and next to new char */
    input_buffer->len++;
    input_buffer->size += strlen(ch);
    return msg_ch; 
}

/* Backspace functionality -- delete preceding character from linked list */
static struct ln_msg_t*
input_buffer_delete(
        struct input_buffer_t* input_buffer,
        struct ln_msg_t* msg_ch)
{
    if (!msg_ch)
        return NULL;

    struct ln_msg_t* res = msg_ch;
    if (msg_ch->next) /* check that we're not deleting null-terminator */
    {
        res = msg_ch->next;
        if (msg_ch->prev) /* there is a char before our char we delete */
        {
            msg_ch->prev->next = msg_ch->next;     /* re-link to skip deleted char */
            msg_ch->next->prev = msg_ch->prev;
        }
        else /* means we delete very first char */
        {
            msg_ch->next->prev = NULL;        /* previous char does not exist */
            input_buffer->msg = msg_ch->next; /* link to next char as first char */
        }
        input_buffer->size -= strlen(msg_ch->ch);
        input_buffer->len--; /* de-crement msg length */
        free(msg_ch);        /* free from memory */
    }
    return res;
}

/* clear linked list from memory */
void
input_buffer_clear()
{
    struct input_buffer_t* input_buffer = get_input_buffer();
    if (!input_buffer->msg)
        return;

    struct ln_msg_t* msg = input_buffer->msg;
    while(msg->next)
    {
        msg = msg->next;
        free(msg->prev);
    }
    input_buffer->len = 1;
    input_buffer->size = 1;
    msg->prev = NULL;
    msg->next = NULL;
    input_buffer->msg = msg;
    return;
}

/* Converts linked char list to char array
 * return amount of bytes written */
int32_t
lnlist_arr(struct input_buffer_t* input_buffer, char* msgarr)
{
    struct ln_msg_t* msg = input_buffer->msg;
    strncpy(msgarr, "\0", BCOM_MAX_MSG_SIZE); /* reset buffer */
    if (!input_buffer->msg)
        return 0;

    int32_t i = 0;
    int32_t j = 0;

    while (msg != NULL)
    {
        for (j = 0; j < strlen(msg->ch); j++)
            msgarr[i + j] = msg->ch[j];

        i += j;

        if (msg->ch[0] == '\0')
            return i;

        msg = msg->next;
        if (i >= BCOM_MAX_MSG_SIZE)
            break;
    }
    return i;
}

static bool
legal_msg_char(char c)
{
    return (c >= 32 && c != 127);
}


/* Checks if character is a
 *      - 0-9 number
 *      - a-z letter
 *      - A-Z letter
 *      - '_' underscore */
static bool
legal_username_char(char c)
{
    return  ((c > 47 && c < 58) ||      /* 0-9 */
             (c > 64 && c < 92) ||      /* a-z */
            (c > 96 && c < 123) ||      /* A-Z */
             (c == 95));                /*  _  */
}

/* Check if username is legal */
static bool
legal_username(char* username)
{
    int32_t counter;
    for (counter = 0; username[counter] != '\0'; counter++)
    {
        if (username[counter] == '\n')
        {
            if (counter < BCOM_MIN_USERNAME_LEN)
                return false;
            username[counter] = '\0';
            return true;
        }
        else if (!legal_username_char(username[counter]))
            return false;
    }
    return true;
}

/* Prompts user to enter username */
int8_t
get_username(struct user_t *user)
{
    ANSI_CLEARLN();
    printf("Enter username: ");
    char buf[BCOM_MAX_USERNAME_LEN];
    if (fgets(buf, BCOM_MAX_USERNAME_LEN, stdin) != NULL)    /* get input from stdin */
    {
        if (legal_username(buf))
        {
            strncpy(user->name, buf, BCOM_MAX_USERNAME_LEN);
            return 1;
        }
    }
    return 0;
}

/* Return appropriate status by analyzing user input */
static struct message_t
command_handler(char* msg)
{
    switch (msg[1])
    {
        case 'q':
            return EMPTY_MSG(BCOM_QUIT);

        default:
            return EMPTY_MSG(BCOM_MSG_EMPTY);
    }
}

/* Handles input characters
 * returns bytes read */
static int32_t
read_input(struct input_buffer_t* input_buffer)
{
    struct ln_msg_t* msg = input_buffer->msg;
    char ch[BCOM_MAX_CHAR_BYTES + 2];
    ssize_t lenin;  /* length of input */

    while (1)
    {
        memset(ch, '\0', BCOM_MAX_CHAR_BYTES + 2);
        lenin = read(STDIN_FILENO, ch, 3);
        if (lenin == -1)
            return -1;
        else if (lenin == 0)
            break;
        else if (lenin == 1)
        {
            switch (ch[0])
            {
                case '\n':
                    return input_buffer->size;
                case '\r':
                    return input_buffer->size;
                case 0x7F:  /* ASCII DEL (Backspace, not Delete) */
                    if (msg->prev)
                        msg = input_buffer_delete(input_buffer, msg->prev);
                    break;
                default:   /* insert char before the cursor */
                    if (legal_msg_char(ch[0]) && iscntrl(ch[0]) == 0 &&
                            (input_buffer->size < BCOM_MAX_MSG_SIZE))
                        msg = input_buffer_insert(input_buffer, msg, ch);
                    break;
            }
        }
        else if (lenin > 1)
        {
            if (!msg)
                continue;
            if (ch[0] == '\033')
            {
                switch (ch[2])
                {
                    case 'A': /* ANSI UP */
                        /* IDK what this should do, bring up some menu maybe? */
                        if (g_cursorUp < BCOM_MAX_MSG_BUF - FILLER_ROWS)
                            g_cursorUp++;
                        break;
                    case 'B': /* ANSI DOWN */
                        if (g_cursorUp > 0)
                            g_cursorUp--;
                        break;
                    case 'C': /* ANSI RIGHT */
                        if (msg->next != NULL)
                        {
                            g_cursorLeftOffset--;
                            msg = msg->next;
                        }
                        break;
                    case 'D': /* ANSI LEFT */
                        if (msg->prev != NULL)
                        {
                            g_cursorLeftOffset++;
                            msg = msg->prev;
                        }
                        break;
                    case '3':
                        if (msg->next)
                            msg = input_buffer_delete(input_buffer, msg);
                        break;
                    default:
                        break;
                }
            }
    /* This was supposed to be handling for backspace */
#if 0
            else if (ch[0] == '^')
            {
                switch (ch[1])
                {
                    case 'H':
                        msg = input_buffer_delete(input_buffer, msg);
                        break;
                    default:
                        break;
                }
            }
#endif
            /* allow printing of 2-byte UNICODE chars */
            else if (lenin == 2 && input_buffer->size + 1 < BCOM_MAX_MSG_SIZE)
            {
                msg = input_buffer_insert(input_buffer, msg, ch);
            }
        }
    }
    /* never gets here */
    return 0;
}

/* gets user input, parses to a message_t and saves it */
void*
get_user_input(struct message_t* message)
{
    struct input_buffer_t* input_buffer = get_input_buffer();
    while (1)
    {
        if (read_input(input_buffer) > 1)    /* get input from stdin */
        {
            g_cursorLeftOffset = 0;
            if (input_buffer->msg->ch[0] == '\0')
            {
                *message = EMPTY_MSG(BCOM_MSG_EMPTY);
                continue;
            }
            if (lnlist_arr(input_buffer, message->msg) == 0)
            { 
                *message = EMPTY_MSG(BCOM_MSG_EMPTY);
                continue;
            }
            message->len = input_buffer->len;
            input_buffer_clear(); 
            if (message->msg[0] == '!') /* message is a command */
            {
                *message = command_handler(message->msg);
                continue;
            }
            message->user = *get_client_user();
            message->status = BCOM_MSG_SET;
            continue;
        }
        *message = EMPTY_MSG(BCOM_MSG_EMPTY);
        input_buffer_clear(); 
    }
    return NULL;
}

