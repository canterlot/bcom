/*
 *
 *
 *
 *
 */


/* System Headers */
#include <pthread.h>
#include <unistd.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <fcntl.h>

/* Local Headers */
#include "bcom_types.h"
#include "server.h"
#include "ui.h"
#include "aes.h"


#define SERVER_STR "@server"


/*******************************************************************************
 * PRIVATE FUNCTIONS                                                           *
 ******************************************************************************/

static void
sent_to_all(int j, int i, int sockfd, int nbytes, char* recv_buf,
        fd_set *master)
{
    if (FD_ISSET(j, master))
    {
        if (j != sockfd && j != 1)
        {
            if (send(j, recv_buf, nbytes, MSG_NOSIGNAL) == -1)
            {
                UI_LPRINTF(SERVER_STR, "Send failed.");
            }
        }
    }
}

static void
send_recv(int i, fd_set *master, int sockfd, int fdmax)
{
    int nbytes, j;
    char data[BCOM_BYTES_PER_COM];

    if ((nbytes = recv(i, data, BCOM_BYTES_PER_COM, 0)) <= 0)
    {
        if (nbytes == 0)
        {
            UI_LPRINTF(SERVER_STR, "Socket %d hung up", i);
        }
        else
        {
            UI_LPRINTF(SERVER_STR, "Recv error.");
        }

        //UI_LPRINTF(SERVER_STR, "Received message");
        close(i);
        FD_CLR(i, master);
    }
    else
    {
        for (j = 0; j <= fdmax; j++)
        {
            sent_to_all(j, i, sockfd, nbytes, data, master);
        }
        //UI_LPRINTF(SERVER_STR, "Message sent back");
    }
}

static void
connection_accept(fd_set *master, int *fdmax, int sockfd,
        struct sockaddr_in *client_addr)
{
    socklen_t addrlen;
    int newsockfd;

    addrlen = sizeof(struct sockaddr_in);
    if ((newsockfd = accept(sockfd, (struct sockaddr *)client_addr,
                    &addrlen)) == -1)
    {
        UI_LPRINTF(SERVER_STR, "Accept error");
    }
    else
    {
        FD_SET(newsockfd, master);
        if (newsockfd > *fdmax)
        {
            *fdmax = newsockfd;
        }
        UI_LPRINTF(SERVER_STR, "New connection from %s on port %d", 
                inet_ntoa(client_addr->sin_addr),
                ntohs(client_addr->sin_port));
    }
}

static void
connect_request(int *sockfd, struct sockaddr_in *my_addr)
{
    int yes = 1;

    if ((*sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        UI_LPRINTF(SERVER_STR, "Socket error");
    }

    my_addr->sin_family = AF_INET;
    my_addr->sin_port = htons(BCOM_PORT);
    my_addr->sin_addr.s_addr = INADDR_ANY;
    memset(my_addr->sin_zero, '\0', sizeof(my_addr->sin_zero));
    
    if (fcntl(*sockfd, F_SETFL, fcntl(*sockfd, F_GETFL, 0) | O_NONBLOCK) == -1)
    {
        UI_LPRINTF(SERVER_STR, "Cannot make socket non-blocking");
    }
    if (setsockopt(*sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
    {
        UI_LPRINTF(SERVER_STR, "Socket options error");
    }
    if (setsockopt(*sockfd, IPPROTO_TCP, TCP_NODELAY, &yes, sizeof(int)) == -1)
    {
        UI_LPRINTF(SERVER_STR, "Socket options error");
    }
    if (setsockopt(*sockfd, IPPROTO_TCP, TCP_QUICKACK, &yes, sizeof(int)) == -1)
    {
        UI_LPRINTF(SERVER_STR, "Socket options error");
    }
    if (bind(*sockfd, (struct sockaddr *)my_addr, sizeof(struct sockaddr)) == 1)
    {
        UI_LPRINTF(SERVER_STR, "Unable to bind");
    }
    if (listen(*sockfd, 10) == -1)
    {
        UI_LPRINTF(SERVER_STR, "Listen error");
    }
    UI_LPRINTF(SERVER_STR, "Waiting for client on port %d", BCOM_PORT);
}


/*******************************************************************************
 * PUBLIC FUNCTIONS                                                            *
 ******************************************************************************/

void *
bcom_server(void *args)
{
    fd_set master;
    fd_set read_fd;
    int fdmax, i;
    int sockfd;
    struct sockaddr_in my_addr, client_addr;
    struct timespec ts = {0, 20}; /* timer */
    struct timeval timeout = {0, 20000};

    sockfd = 0;

    FD_ZERO(&master);
    FD_ZERO(&read_fd);
    connect_request(&sockfd, &my_addr);
    FD_SET(sockfd, &master);

    fdmax = sockfd;
    while (1)
    {
        read_fd = master;
        if (select(fdmax + 1, &read_fd, NULL, NULL, &timeout) == -1)
        {
            UI_LPRINTF(SERVER_STR, "Select error");
        }

        for (i = 0; i <= fdmax; i++)
        {
            if (FD_ISSET(i, &read_fd))
            {
                if (i == sockfd)
                {
                    connection_accept(&master, &fdmax, sockfd, &client_addr);
                }
                else
                {
                    send_recv(i, &master, sockfd, fdmax);
                }
            }
            nanosleep(&ts, NULL);
        }
        nanosleep(&ts, NULL);
    }
    return NULL;
}
